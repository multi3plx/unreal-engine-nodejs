
//Using the HiveMQ public Broker, with a random client Id
var client = new Messaging.Client("101.167.175.252", 8000, "myclientid_" + parseInt(Math.random() * 100, 10));
//Gets  called if the websocket/mqtt connection gets disconnected for any reason
client.onConnectionLost = function (responseObject) {
     //Depending on your scenario you could implement a reconnect logic here
     alert("connection lost: " + responseObject.errorMessage);
 };
 //Gets called whenever you receive a message for your subscriptions
 client.onMessageArrived = function (message) {
     //Do something with the push message you received
     $('#messages').append('<span>Topic: ' + message.destinationName + '  | ' + message.payloadString + '</span><br/>');
     console.log(message.payloadString);
     var payload = JSON.parse(message.payloadString);
     if(payload) {
     	if(message.destinationName.includes("ktpower"))
     	{
				var power = db.collection("powerUsage").find();
		    power.each(function(err, doc) {
		    	console.log(err);
		        console.log(doc);

		    });
 		    var powerTotal = {
		      KPU: payload.power
		    };
		    var powerUsage = db.collection('powerUsage');  
		    powerUsage.insert(testUser, function(err, docs) {
		    if (err) {
		        throw err;
		      } else {
		        console.log(docs);
		      }
		  });

			document.getElementById("KitchenPU").innerHTML = payload.power + " KW/h";
     	}
     	if(message.destinationName.includes("spower"))
     	{
			document.getElementById("ServerPU").innerHTML = payload.power + " KW/h";
     	}
     	if(message.destinationName.includes("lpower"))
     	{
			document.getElementById("LightingU").innerHTML = payload.power + " KW/h";
     	}
     	if(message.destinationName.includes("loungeac"))
     	{
			document.getElementById("LoungeAC").innerHTML = payload.power + " KW/h";
     	}
     	if(message.destinationName.includes("bedac"))
     	{
			document.getElementById("BedAC").innerHTML = payload.power + " KW/h";
     	}
     }
 };

 //Connect Options
 var options = {
     timeout: 3,
     //Gets Called if the connection has sucessfully been established
     onSuccess: function () {
         client.subscribe('#', {qos: 2});

     },
     //Gets Called if the connection could not be established
     onFailure: function (message) {
         alert("Connection failed: " + message.errorMessage);
     }
 };

 //Creates a new Messaging.Message Object and sends it to the HiveMQ MQTT Broker
 var publish = function (payload, topic, qos) {
 	var stringifyPayload = JSON.stringify(payload);
     //Send your message (also possible to serialize it as JSON or protobuf or just use a string, no limitations)
     var message = new Messaging.Message(stringifyPayload);
     message.destinationName = topic;
     message.qos = 2;
     client.send(message);
 }

client.connect(options);
	

$('#onselectExample').timepicker();
$('#roundTimeExample').on('changeTime', function() {
    $('#onselectTarget').text($(this).val());
});

$('#roundTimeExample').timepicker({ 'forceRoundTime': true });

function set_payload()
	{
	var text =   '{ "days": [{ ' +
			'"mon": "0", '+
			'"tue": "0", '+
			'"wed": "0",'+
			'"thu": "0",'+
			'"fri": "0",'+
			'"sat": "0",'+
			'"sun": "0"' +
			'},{' +
			'"duration": "0",'+
			'"time": "0500"' +
		'}]}'
		
	  var payload = JSON.parse(text);

	  if (document.getElementById('mon').checked) 
	  {
	   	  payload.days[0].mon = 1;
	  }
	  if(document.getElementById('tue').checked)  
	  {
	      payload.days[0].tue = 1;

	  }
	  if(document.getElementById('wed').checked)  
	  {
	      payload.days[0].wed = 1;
	  }
	  if(document.getElementById('thu').checked)  
	  {
	      payload.days[0].thu = 1;
	  }
	  if(document.getElementById('fri').checked)  
	  {
	      payload.days[0].fri = 1;
	  }					  
	  if(document.getElementById('sat').checked)  
	  {
	      payload.days[0].sat = 1;
	  }
	  if(document.getElementById('sun').checked)  
	  {
	      payload.days[0].sun = 1;
	  }

  	  payload.days[1].time = document.getElementById('roundTime').value;
   	  payload.days[1].duration = document.getElementById('roundTimeDuration').value;
	  publish(payload, 'irrigation/stations', 2);
	}


function reset_payload() 
	{
		var text =   '{ "days": [{ ' +
			'"mon": "0", '+
			'"tue": "0", '+
			'"wed": "0",'+
			'"thu": "0",'+
			'"fri": "0",'+
			'"sat": "0",'+
			'"sun": "0"' +
			'},{' +
			'"duration": "0",'+
			'"time": "0500"' +
		'}]}'
		
	  var payload = JSON.parse(text);
	  publish(payload, 'irrigation/stations', 2);
	}