Unreal-Engine Node.js API for Communication to Games/Mods
====================

This should get you setup with a basic Node.js & MongoDB application on Ubuntu , Sorry i wont go into how to setup for other OS's.


Prerequisites
================================


* NodeJS v8.10.0


	* sudo apt update
	
	* sudo apt install nodejs

	* sudo apt install npm

	* nodejs -v to check correct installation


* MongoDB v3.6.3


	* sudo npm install mongoose

	* mongod --version


# Getting Started

You will need to setup your initial authentication details as a document in mongodb. I've left an easy way commented on lines 1057 & 108. Just run the application once with those lines uncommented and that will be your authentication details for basic auth.
Go to the API directory and run npm start. Go to http://localhost:8040/npc too see the example page.

*Note ports will need to be fowarded etc default port is 8040*

## API Demo

http://warpstudios.com.au:8040/npc

![Alt text](https://bitbucket.org/multi3plx/unreal-engine-nodejs/raw/811a45f21138114c8b6fd1ce7c675b9f802fc551/images/apiexample.PNG)

## UE4 Blueprint example

![Alt text](https://bitbucket.org/multi3plx/unreal-engine-nodejs/raw/811a45f21138114c8b6fd1ce7c675b9f802fc551/images/ue4example.PNG)
