var express = require('express');
var router = express.Router();
var http = require('http');  
var url = require('url');  
var basicAuth = require('basic-auth');
var nodemailer = require('nodemailer');
var mongoose = require("mongoose");
const mongooseFieldEncryption = require("mongoose-field-encryption").fieldEncryption;
const Schema = mongoose.Schema;



mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/local", { useNewUrlParser: true });
mongoose.set('useFindAndModify', false);

var message;
var messageArray = [];

var npcs = [];
var playerKills = [];
var playerReps = [];

var npcKillFeeds = [];
var playerKillFeeds = [];
var npcNetworths = [];


var networth = 0;
var kills = 0;
var rep = 0;
var pkills = 0;
var options = {
    new: true,
    upsert: true
  }

//Encrypted Authentication for API 
const authentication = new Schema({
  user: String,
  password: String
});

// Store secret key somewhere
authentication.plugin(mongooseFieldEncryption, {
  fields: ["user", "password"],
  secret: "secretkey",
  saltGenerator: function(secret) {
    return "1234567890123456"; // should ideally use the secret to return a string of length 16
  }
});

const Post = mongoose.model("auth", authentication);

// DB SCHEMA PF
var npcKill = new mongoose.Schema({
  name: String,
  kills: String
})

var playerKill = new mongoose.Schema({
  name: String,
  kills: String
})

var playerReputation = new mongoose.Schema({
  name: String,
  reputation: String
})

var npcKillFeed = new mongoose.Schema({
  name: String,
  killed: String
})

var playerKillFeed = new mongoose.Schema({
  name: String,
  killed: String
})

var npcNetworth = new mongoose.Schema({
  name: String,
  networth: String
})

var npcKillFeed = mongoose.model("npcKillFeed", npcKillFeed);
var playerKillFeed = mongoose.model("playerKillFeed", playerKillFeed);
var npcNetworth = mongoose.model("npcNetworth", npcNetworth);
var serverCounter = mongoose.model("serverCounter", serverCounter);
var npcKill = mongoose.model("npcKill", npcKill);
var playerKill = mongoose.model("playerKill", playerKill);
var playerReputation = mongoose.model("playerReputation", playerReputation);



var auth = async function(req, res, next) {

  // Requires basic auth
  var user = basicAuth(req);
  if (!user || !user.name || !user.pass) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    res.sendStatus(401);
    return;
  }

  // Run this once somewhere to add a user without having to use db terminal
  //const post = new Post({ user: "admin", password: "admin" });
  //await post.save();

  //Search for encrypted username
  const userToSearchFor = new Post({ user: user.name });
  userToSearchFor.encryptFieldsSync();
  const results = await Post.find({ user: userToSearchFor.user });

  // Verify authentication
  if (user.name === results[0].user && user.pass === results[0].password) {
    next();
  } else {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    res.sendStatus(401);
    return;
  }
}


// NPC STUFF
router.all("/npc", async (req, res) => {

    await playerReputation.find().sort({reputation: 1}).limit(20).exec(function(err, result) {
      playerReps = [];
      playerReps.push(result);
    });

    await npcKillFeed.find().sort({kills: 1}).limit(20).exec(function(err, result) {
    npcKillFeeds = [];
    npcKillFeeds.push(result);
  });  

      await playerKillFeed.find().sort({kills: 1}).limit(20).exec(function(err, result) {
    playerKillFeeds = [];
    playerKillFeeds.push(result);
  });

      await npcNetworth.find().sort( {networth: -1} ).limit(1).exec(function(err, result) {
  npcNetworths = [];
  npcNetworths.push(result);
});

    await playerKill.find().sort({kills: 1}).limit(20).exec(function(err, result) {
    playerKills = [];
    playerKills.push(result);
  });

    await npcKill.find().sort({kills: 1}).limit(20).exec(function(err, result) {
        npcs = [];
        npcs.push(result);
        res.render('npc', {title: 'NPC Statistics and Leaderboards', _npcs: npcs, _playrep: playerReps, _playkills: playerKills,
      _npcfeedkills: npcKillFeeds, _npcworth: npcNetworths, _playfeedkills: playerKillFeeds});
    });
});

// Add NPC KILL
router.post("/addnpckill",  async (req, res) => {
  //var myData = new npcKill(req.body);
  //await myData.save()
    var exists = false;
    const filter = { name: req.body.name };
    await npcKill.findOne({ name: req.body.name}, function (err, doc){
  // doc is a Document
  if(doc) {
    kills = ((doc.kills - 0) + (req.body.kills - 0));
    exists = true;
  } else {
    kills = 1;
    exists = false;
  }
});
  const updatedKills = kills;
  const update = { kills: (updatedKills - 0) };
  await npcKill.findOneAndUpdate(filter, update, options, function (err, doc){
  });


  res.status(200).send('Added');
});

// Add Player KILL
router.post("/addplayerkill", async (req, res) => {
  //var myData = new npcKill(req.body);
  //await myData.save()
    var exists = false;
    const filter = { name: req.body.name };
    await playerKill.findOne({ name: req.body.name}, function (err, doc){
  // doc is a Document
  if(doc) {
    kills = ((doc.kills - 0) + (req.body.kills - 0));
    exists = true;
  } else {
    kills = 1;
    exists = false;
  }
});
  const updatedKills = kills;
  const update = { kills: (updatedKills - 0) };
  await playerKill.findOneAndUpdate(filter, update, options, function (err, doc){
  });

  res.status(200).send('Added');
});

// Add Player Reputation
router.post("/addplayrep", async (req, res) => {
  //var myData = new npcKill(req.body);
  //await myData.save()
    var exists = false;
    const filter = { name: req.body.name };
    await playerReputation.findOne({ name: req.body.name}, function (err, doc){
  // doc is a Document
  if(doc) {
    rep = ((doc.reputation - 0) + (req.body.reputation - 0));
    exists = true;
  } else {
    rep = 1;
    exists = false;
  }
});
  const updatedRep = rep;
  const update = { reputation: (updatedRep - 0) };
  await playerReputation.findOneAndUpdate(filter, update, options, function (err, doc){
  });

  res.status(200).send('Added');
});

// Decrease Player Reputation
router.post("/removeplayrep", async (req, res) => {
  //var myData = new npcKill(req.body);
  //await myData.save()
    var exists = false;
    const filter = { name: req.body.name };
    await playerReputation.findOne({ name: req.body.name}, function (err, doc){
  // doc is a Document
  if(doc) {
    rep = ((doc.reputation - 0) - (req.body.reputation - 0));
    exists = true;
  } else {
    rep = 1;
    exists = false;
  }
});
  const updatedRep = rep;
  const update = { reputation: (updatedRep - 0) };
  await playerReputation.findOneAndUpdate(filter, update, options, function (err, doc){
  });

  res.status(200).send('Added');
});

// Add Player Kill Feed
router.post("/addplayerkillfeed",  async (req, res) => {
  var myData = new playerKillFeed(req.body);
  await myData.save()

  res.status(200).send('Added');
});

// Add NPC Kill Feed
router.post("/addnpckillfeed",  async (req, res) => {
  var myData = new npcKillFeed(req.body);
  await myData.save()

  res.status(200).send('Added');
});

// Add NPC Networth
router.post("/addnpcnetworth", async (req, res) => {
    var exists = false;
    const filter = { name: req.body.name };
  networth = req.body.networth;
  const updatedNetworth = networth;
  const update = { networth: (updatedNetworth - 0) };
  await npcNetworth.findOneAndUpdate(filter, update, options, function (err, doc){
  });

  res.status(200).send('Added');
});

module.exports = router;